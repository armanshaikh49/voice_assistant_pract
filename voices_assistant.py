import speech_recognition as sr
from time import ctime
import time
import os
from gtts import gTTS


def speak(audio):
    print(audio)  
    tts = gTTS(text=audio, lang='en-in')
    tts.save("audio.mp3")
    os.system("mpg321 audio .mp3")

def recordAudio():
    
    #Recording Audio
    r = sr.Recognizer()
    with sr.Microphone() as source:
        # print('say something')
        audio = r.listen(source)

    #speech recognition using google speech Recognition
    data = ""
    try:
        data = r.recognize_google(audio)
        print = ("you said" + data)
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))

    return data

def jarvis(data):
    if 'how are you' in data:
        speak("i am fine")
    
    if "time is it" in data:
        speak(ctime())

time.sleep(2)
speak("Hiii, Armaan what can i do for you")

while 1:
    data = recordAudio()
    jarvis(data)

